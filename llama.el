;;; llama.el --- a package for LLaMa Model                    -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Hector Orlando Crispens

;; Author: Hector Orlando Crispens <hector.or.cr@gmail.com>
;; Keywords: lisp, llama, IA
;; Version: 1.0.0
;; URL: https://gitlab.com/hector.or.cr/llama-emacs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; El siguiente paquete provee una interface para la instalación de LLaMa 2 en emcas que permite interactuar con este modelo de inteligencia artificial a través de un buffer.
;; Llama 2, un modelo de lenguaje de gran tamaño, es producto de una alianza poco común entre Meta y Microsoft,en la investigación en inteligencia artificial.
;; Es un sucesor del modelo de lenguaje Llama 1 de Meta, lanzado en el primer trimestre de 2023.

;; The next s a package that provides a interface for LLaMa 2 installation in Emacs for allowing interaction with this artificial intelligence model without a buffer.
;; Llama 2, a large language model, is a product of an uncommon alliance between Meta and Microsoft, two competing tech giants at the forefront of artificial intelligence research.
;; It is a successor to Meta's Llama 1 language model, released in the first quarter of 2023.

;;; Usage:

;; [Configuration] Add next to your .emacs
;; (1) (add-to-list 'load-path "~/.emacs.d/packages") ;; Add package path to list
;; (2) (load "~/.emacs.d/packages/llama") ;; Load package
;; (3) (llama-start) ;; Start LLaMa 2
;; (4) (global-set-key (kbd "C-x l") 'ask) ;; Ask for LLaMa 2

;; [Install] re open if neccesary
;; (5) M-x llama-install ;; Install LLaMa 2
;; (6) M-x llama-download-model ;; Download LLaMa 2 model

;; [Usage] Ask for LLaMa 2
;; (7) M-x ask or C-x l ;; Ask for LLaMa 2

;;; Code 


(defvar llama-args "--color --ctx_size 2048 -n -1 -ins -b 256 --top_k 10000 --temp 0.2 --repeat_penalty 1.1 -t 8\n")
(defvar llama-process "llama-process")
(defvar llama-process-buffer "llama-process-buffer")

(defvar llama-repo "https://github.com/ggerganov/llama.cpp.git")
(defvar llama-buffer-instalacion "LlaMa-2 instalacion")

(defvar model-url "https://huggingface.co/TheBloke/Llama-2-13B-chat-GGML/resolve/main/")
(defvar model-name "llama-2-7b-chat.ggmlv3.q4_1.bin")

(defun llama-install ()
  (interactive)
  (let ((llama-dir (concat (getenv "HOME") "/.LlaMa-2/")))
  (delete-directory llama-dir t)
  (make-directory llama-dir)
  ;;  (shell-command (concat "git clone " llama-repo " " llama-dir))
  (message "%s" "cloning the repository...")
  (call-process "git" nil llama-buffer-instalacion nil "clone" llama-repo llama-dir)
  ;;(make-directory (concat llama-dir "models/"))
  ;;(shell-command (concat "wget -O " (concat llama-dir "models/image.jpg") " " model-url))
  (shell-command (concat "cd " llama-dir " && git checkout a113689"))
  (message "%s" "start to compile project...")
  (call-process "make" nil "bar" nil "-C" (concat  (getenv "HOME") "/.LlaMa-2/"))
  (message "%s" "finish to install, now you should download the model using the command llama-download-model...")
  ))

(defun llama-uninstall ()
  (interactive)
  (let ((llama-dir (concat (getenv "HOME") "/.LlaMa-2/")))
    (delete-directory llama-dir t)
    (message "%s" "finish to uninstall...")
  )
  )

(defun llama-download-model ()
  (interactive)
  (if (file-exists-p (concat (getenv "HOME") "/.LlaMa-2/models/" model-name))
      (message "%s" "El modelo ya existe y se encuentra descargado")
    (progn 
  (message "%s %s" "downloading the model" (concat " " model-name " ..."))
  (call-process "wget" nil llama-buffer-instalacion nil "-O" (concat  (getenv "HOME") "/.LlaMa-2/models/" model-name) (concat model-url model-name))
  ))
  )

(defun llama-start ()
  "Run external system programs. Dmenu/Rofi-like.  Tab/C-M-i to completion
n-[b/p] for walk backward/forward early commands history."
  (interactive)
  (require 'subr-x)
  (let ((llama-dir (concat (getenv "HOME") "/.LlaMa-2/")))
    (if (file-exists-p (concat (getenv "HOME") "/.LlaMa-2/models/" model-name))
	(progn (start-process-shell-command llama-process llama-process-buffer (concat llama-dir "./main -m" " " llama-dir "models/" model-name " " llama-args))
	       (set-process-filter (get-process llama-process) 'llama-filter))
      (if (y-or-n-p "Download it?")
	  (call-process "wget" nil llama-buffer-instalacion nil "-O" (concat  (getenv "HOME") "/.LlaMa-2/models/" model-name) (concat model-url model-name))
	(message "%s" "Model not found"))
      );; close if
    );; close let
  );; close function


(defun ask (ask)
  (interactive "sWhat do you ask?: ")
  "send string to process"
  (switch-to-buffer llama-process-buffer)
  (process-send-string llama-process (concat ask "\n"))
  (process-send-eof llama-process)
  )



(defun llama-filter (proc string)
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (let ((moving (= (point) (process-mark proc))))

        (save-excursion
          ;; Insert the text, advancing the process marker.
          (goto-char (process-mark proc))
          (insert (replace-regexp-in-string "\^\[\[m[0-9]+" "" string))
          (set-marker (process-mark proc) (point)))
        (if moving (goto-char (process-mark proc)))))))

  
  
(provide 'llama-install)

;; Local Variables:
;; coding: utf-8
;; End:

;;; llama.el ends here
