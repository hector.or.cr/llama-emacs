![logo](./images/logo.png)

# LlaMa emacs

## Introducción

Los modelos de lenguaje grande (LLM) son un tipo de modelo de inteligencia artificial (IA) que se entrena con grandes cantidades de datos de texto para generar resultados de lenguaje que sean coherentes y que suenen naturales. Estos modelos se han vuelto cada vez más populares en los últimos años debido a su capacidad para generar texto que a menudo es indistinguible del texto escrito por humanos.

Los LLM generalmente se entrenan utilizando técnicas de aprendizaje profundo, como transformadores o redes neuronales recurrentes (RNN), en grandes conjuntos de datos de texto. Durante el entrenamiento, el modelo aprende a predecir la siguiente palabra en una secuencia de texto dado el contexto de las palabras anteriores. Este proceso se repite millones de veces y el modelo ajusta sus predicciones en función de la retroalimentación que recibe de los datos de entrenamiento.

La ventaja clave de los LLM es su capacidad para generar texto coherente y que suene natural. A diferencia de los modelos de lenguaje más pequeños que pueden tener dificultades para generar texto que sea gramaticalmente correcto o contextualmente apropiado, los LLM pueden producir texto que a menudo es indistinguible del texto escrito por humanos.

Algunos ejemplos de los tipos de tareas que pueden realizar los LLM incluyen:

1. Traducción de idiomas: los LLM pueden capacitarse para traducir textos de un idioma a otro.
2. Generación de texto: los LLM se pueden utilizar para generar texto nuevo basado en un tema o tema determinado.
3. Comprensión del lenguaje: los LLM pueden capacitarse para comprender el significado del texto y responder adecuadamente.
4. Chatbots: los LLM se pueden utilizar para impulsar chatbots que puedan entablar conversaciones con humanos.
5. Creación de contenido: los LLM se pueden utilizar para generar contenido, como artículos, publicaciones de blogs o actualizaciones de redes sociales.

A pesar de sus muchas ventajas, los LLM no están exentos de limitaciones. Requieren grandes cantidades de datos de entrenamiento y recursos computacionales para entrenar, y pueden ser difíciles de interpretar y comprender. Además, es posible que los LLM no siempre produzcan un texto que sea preciso o apropiado, especialmente si los datos de capacitación están sesgados o están incompletos.

En general, los LLM representan un avance significativo en el campo del procesamiento del lenguaje natural y tienen el potencial de revolucionar muchas industrias y aplicaciones. Sin embargo, es importante considerar cuidadosamente las limitaciones y los riesgos potenciales asociados con estos modelos para garantizar que se utilicen de manera responsable y ética.



## Descripción de LlaMa emacs

Este paquete permite instalar e integrar al editor de código emacs un **large language model** (LLM) para interactuar con éste a través de un buffer permitiendo tener dentro del editor un chat asistido como chatGPT pero completamente **Open Source**. Cabe destacar que LlaMa emacs descarga el modelo pre-entrenado de LlaMa 2 para interactuar con este de forma offline y permitir que éste asista al usuario sin necesidad de una conexión a internet. Dentro de las diferentes configuraciones de LlaMa 2, por defecto este paquete viene configurado para trabajar con el modelo `llama-2-7b-chat.ggmlv3.q4_1.bin` por una cuestión de performance, sin embargo es posible trabajar con otras variantes de este modelo modificando la variable `model-name` dentro de este package.

  [![linux](https://img.shields.io/badge/Linux-5.10.0--26-FFC624?style=plastic&logo=linux)](https://www.kernel.org/)   [![debian](https://img.shields.io/badge/Debian-11-A81D33?style=plastic&logo=debian)](https://www.debian.org/)    [![emacs](https://img.shields.io/badge/GNU_Emacs-27.1-7F5AB6?style=plastic&logo=gnuemacs)](https://www.gnu.org/software/emacs/)    [![meta](https://img.shields.io/badge/Meta-LLMs-0467DF?style=plastic&logo=meta)](https://ai.meta.com/llama/#download-the-model)

## Contributors

[![hector](https://raw.githubusercontent.com/hectorcrispens/dockAngl/master/img/avatar-hector.svg)](https://www.linkedin.com/in/hector-orlando-25124a18a/)  Héctor Orlando, Crispens

- [![linkedin](https://img.shields.io/badge/GitHub--0a66c2?style=social&logo=GitHub)](https://github.com/hectorcrispens)  [![linkedin](https://img.shields.io/badge/GitHub--FC6D26?style=social&logo=GitLab)](https://gitlab.com/hector.or.cr) 
- [![linkedin](https://img.shields.io/badge/Gmail--0a66c2?style=social&logo=Gmail)](mailto:hector.or.cr@gmail.com) [![linkedin](https://img.shields.io/badge/LinkedIn--0a66c2?style=social&logo=linkedin)](https://www.linkedin.com/in/hector-orlando-25124a18a/)

Versión v1.0.0

## Tabla de contenido

[TOC]



## Instalación

La instalación del paquete como también del modelo se resuelve en 3 *(tres)* pasos que se detallan a continuación:

1. **La instalación de este paquete**:  Es bastante sencilla,  se recomienda descargar el archivo `.el` y colocarlo en la siguiente ubicación `/home/$USER/.emacs.d/packages`. Una vez hecho esto se necesita agregar el `path` al archivo de configuración de **emacs**, abrir el archivo y configurar alguno de los atajos de teclado para interactuar en el model. La siguiente configuración establece `ctrl + x l` para interactuar con LlaMa.

   ```lisp
   (add-to-list 'load-path "~/.emacs.d/packages")
   
   (load "~/.emacs.d/packages/llama")
   
   (llama-start)
   
   (global-set-key (kbd "C-x l") 'ask)
   ```

   
2. **Instalación y compilación del binario**:  Una vez realizada la configuración se debe reiniciar emacs para que tome la nueva configuración y podamos disponer de las funciones del paquete. A conitnuación se debe ejecutar la función `llama-install`.

   ```lisp
   M-x llama-install
   ```

   Esto comenzará con la instalación de los binarios necesarios para interactuar con **LlaMa**. Se creará el directorio `/home/$USER/.LlaMa-2/`. Allí se compilarán los archivos. 

3. **Descargar el modelo**: Por último se debe descargar el modelo. Como se menciona mas arriba el modelo por defecto por una cuestión de performance es `llama-2-7b-chat.ggmlv3.q4_1.bin` pero se puede modificar el archivo `llama.el` y particularmente la variable `llama-model` para trabajar con otra de las variantes del modelo. 

   Cabe destacar que el modelo se descarga de [huggingface](https://huggingface.co/TheBloke/Llama-2-13B-chat-GGML), alli encontrará otras variantes del modelo para instalar si lo desea.

   Para descargar el modelo ejecutar el siguiente comando dentro de **emacs**:

   ```lisp
   M-x llama-download-model
   ```

   Esto hará que el modelo comience a descargar y se guarde dentro de la carpeta `/home/$USER/.LlaMa-2/models`.

> Es importante luego de descargado el model, que nuevamente se reinicie emacs o en su defecto ejecutar la función llama-start

## Uso

Una vez instalados los binarios y descargado el modelo ya es posible interactuar con llama a través a través de la linea de comandos de **emacs**. La función que permite interactuar con llama es la funcion **ask** y el atajo de teclado para invocar esta funcion es el que se haya configurado en el archivo `.emacs` pero por defecto es `C-x l`.

```lisp
M-x ask
;; o
C-x l
```

Esto nos solicitará que consultemos y nos aparecerá el siguiente mensaje:

```ABAP
What do you ask?:
```

donde podremos consultar al modelo e interactuar.

La función `llama-start` que configuramos en el archivo `.emacs` permite iniciar la interación con el modelo y automáticamente se crea un buffer llamado **llama-process-buffer**. Este buffer es muy importante ya que constituye la salida estandar del modelo y donde podremos obtener las respuestas de LlaMa.

#### asking

![ask](./images/llama-ask.png)

#### responding

![responding](./images/llama-answer.png)





## Licencia

### GNU General Public License

Este programa es software gratuito: puedes redistribuirlo y/o  modificar bajo los términos de la Licencia Pública General GNU tal como  se publicó por la Free Software Foundation, ya sea la versión 3 de la  Licencia, o cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea de utilidad,  pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de  COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR. Ver el Licencia  pública general GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General GNU junto con este programa, en LICENSE.md o https://www.gnu.org/licenses/gpl-3.0.html.en.
